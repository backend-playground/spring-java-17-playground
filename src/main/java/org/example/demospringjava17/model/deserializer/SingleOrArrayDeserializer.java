package org.example.demospringjava17.model.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SingleOrArrayDeserializer extends JsonDeserializer<List<Integer>> {

    @Override
    public List<Integer> deserialize(JsonParser jp, DeserializationContext ctx)
            throws IOException {
        ObjectMapper mapper = (ObjectMapper) jp.getCodec();
        JsonNode node = mapper.readTree(jp);

        List<Integer> ids = new ArrayList<>();
        if (node.isArray()) {
            for (JsonNode elementNode : node) {
                ids.add(elementNode.asInt());
            }
        } else if (node.isInt()) {
            ids.add(node.asInt());
        }
        return ids;
    }
}
