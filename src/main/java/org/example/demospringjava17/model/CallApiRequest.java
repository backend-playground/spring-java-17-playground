package org.example.demospringjava17.model;

import lombok.Data;

import java.util.Map;
import java.util.Objects;

@Data
public class CallApiRequest {
    private String url;
    private String method;
    private Object body;
    private Map<String,String> headers;
    private Map<String,String> params;
}
