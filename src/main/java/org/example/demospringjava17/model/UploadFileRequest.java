package org.example.demospringjava17.model;

import lombok.Data;

@Data
public class UploadFileRequest {
    private String name;
    private int id;

}
