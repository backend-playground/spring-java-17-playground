package org.example.demospringjava17.model;

import lombok.Data;
import org.example.demospringjava17.model.deserializer.SingleOrArrayDeserializer;

import java.util.List;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Data
public class SingleOrArrayModel {

    @JsonDeserialize(using = SingleOrArrayDeserializer.class)
    private List<Integer> portalJobId;
}
