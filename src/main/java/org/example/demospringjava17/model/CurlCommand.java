package org.example.demospringjava17.model;

import lombok.Data;

@Data
public class CurlCommand {
    private String command;
}
