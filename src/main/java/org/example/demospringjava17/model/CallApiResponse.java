package org.example.demospringjava17.model;

import lombok.Data;

import java.util.Map;

@Data
public class CallApiResponse {
    private CallApiRequest request;
    private ResponseDetail response;



    @Data
    public static class ResponseDetail {
        private String code;
        private String data;
        private Map<String,String> headers;
    }
}
