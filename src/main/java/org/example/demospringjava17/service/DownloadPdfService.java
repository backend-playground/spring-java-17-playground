package org.example.demospringjava17.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

@Service
@Slf4j
public class DownloadPdfService {

    public String downloadExistingPdfFile()  {
        // Load PDF file from the classpath
        ClassPathResource pdfFile = new ClassPathResource("example.pdf");

        // Read PDF file content
        byte[] pdfBytes = new byte[0];
        try {
            pdfBytes = Files.readAllBytes(pdfFile.getFile().toPath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Encode PDF content to Base64
        return Base64.getEncoder().encodeToString(pdfBytes);

    }

    public byte[] downloadExistingPdfFileAsByte()  {
        // Load PDF file from the classpath
        ClassPathResource pdfFile = new ClassPathResource("example.pdf");

        // Read PDF file content
        byte[] pdfBytes = new byte[0];
        try {
            pdfBytes = Files.readAllBytes(pdfFile.getFile().toPath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Encode PDF content to Base64
        return pdfBytes;

    }
}
