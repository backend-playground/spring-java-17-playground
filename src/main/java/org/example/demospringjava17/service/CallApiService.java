package org.example.demospringjava17.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManagerBuilder;
import org.apache.hc.client5.http.ssl.NoopHostnameVerifier;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactoryBuilder;
import org.apache.hc.core5.ssl.SSLContextBuilder;
import org.apache.hc.core5.ssl.TrustStrategy;
import org.example.demospringjava17.model.CallApiRequest;
import org.example.demospringjava17.model.CallApiResponse;
import org.springframework.http.*;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
public class CallApiService {

    public CallApiResponse callApi(CallApiRequest req) {
        ResponseEntity<String> response;
        CallApiResponse res = new CallApiResponse();
        CallApiResponse.ResponseDetail responseDetail = new CallApiResponse.ResponseDetail();
        Map<String, String> responseHeaders = new HashMap<>();
        ;
        try {
            MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
            if (Objects.nonNull(req.getParams()))
                req.getParams().forEach(queryParams::add);

            String url = UriComponentsBuilder.fromHttpUrl(req.getUrl())
                    .queryParams(queryParams)
                    .build()
                    .toUriString();

            String body = objectToString(req.getBody());
            log.info(body);
//            body = body.substring(1,body.length()-1);
            body = body.replace("\\","");
            log.info(body);
            HttpEntity<String> httpEntity = new HttpEntity<>(body, createHttpHeaders(req.getHeaders()));
            RestTemplate restTemplate = checkEndpoint(req.getUrl());
            response = restTemplate.exchange(url, getHttpMethod(req.getMethod()), httpEntity, String.class);

            response.getHeaders().forEach((k, v) -> responseHeaders.put(k, String.join(",", v)));
            responseDetail.setCode(String.valueOf(response.getStatusCode().value()));
            responseDetail.setData(response.getBody());
            responseDetail.setHeaders(responseHeaders);
        } catch (RestClientException e) {
            responseDetail.setData(e.getMessage());
        }

        res.setRequest(req);
        res.setResponse(responseDetail);
        return res;
    }

    private HttpHeaders createHttpHeaders(Map<String, String> headers) {
        HttpHeaders httpHeader = new HttpHeaders();
        httpHeader.setContentType(MediaType.APPLICATION_JSON);
        headers.forEach(httpHeader::set);
        return httpHeader;
    }

    private HttpMethod getHttpMethod(String method) {
        return switch (method) {
            case "GET" -> HttpMethod.GET;
            case "POST" -> HttpMethod.POST;
            case "PUT" -> HttpMethod.PUT;
            case "PATCH" -> HttpMethod.PATCH;
            case "HEAD" -> HttpMethod.HEAD;
            case "OPTIONS" -> HttpMethod.OPTIONS;
            case "TRACE" -> HttpMethod.TRACE;
            default -> HttpMethod.GET;
        };
    }

    ObjectMapper objectMapper = new ObjectMapper();

    public String objectToString(Object req) {
        String jsonReq = "";
        try {
            jsonReq = objectMapper.writeValueAsString(req);
        } catch (Exception e) {
            log.error("objectToString Exception = {}", e.getMessage());
        }
        return jsonReq;
    }

    private final int webClientTimeout = 180000;

    public RestTemplate checkEndpoint(String url) {
        RestTemplate restTemplate = new RestTemplate();
        if (url.startsWith("https")) {
            restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(getClientHttpsRequestFactorySkipCert()));
        } else {
            restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(getClientHttpRequestFactory()));
        }
        return restTemplate;
    }

    private ClientHttpRequestFactory getClientHttpsRequestFactorySkipCert() {
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClients.custom()
                    .setConnectionManager(PoolingHttpClientConnectionManagerBuilder.create()
                            .setSSLSocketFactory(SSLConnectionSocketFactoryBuilder.create()
                                    .setSslContext(new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
                                        public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                                            return true;
                                        }
                                    }).build())
                                    .setHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                                    .build())
                            .build())
                    .build();
        } catch (KeyManagementException e) {
            log.error("KeyManagementException in creating http client instance", e);
        } catch (NoSuchAlgorithmException e) {
            log.error("NoSuchAlgorithmException in creating http client instance", e);
        } catch (KeyStoreException e) {
            log.error("KeyStoreException in creating http client instance", e);
        }
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        requestFactory.setConnectTimeout(this.webClientTimeout);
//        requestFactory.setReadTimeout(this.webClientTimeout);
        requestFactory.setConnectionRequestTimeout(this.webClientTimeout);
        return requestFactory;
    }

    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(this.webClientTimeout);
//        clientHttpRequestFactory.setReadTimeout(this.webClientTimeout);
        clientHttpRequestFactory.setConnectionRequestTimeout(this.webClientTimeout);
        return clientHttpRequestFactory;
    }
}
