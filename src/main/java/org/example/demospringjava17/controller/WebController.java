package org.example.demospringjava17.controller;

import org.example.demospringjava17.model.CurlCommand;
import org.example.demospringjava17.service.ParseCurlCommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class WebController {

    @Autowired
    private ParseCurlCommandService parseCurlCommandService;

    @GetMapping("/hi")
    public String hi(){
        return "Hi";
    }

    @PostMapping("/curl")
    public Map<String, String> curl(@RequestBody CurlCommand curlCommand){
        System.out.println(curlCommand.getCommand());
        Map<String, String> components = parseCurlCommandService.parseCurlCommand(curlCommand.getCommand());
        System.out.println("HTTP Method: " + components.get("method"));
        System.out.println("URL: " + components.get("url"));
        System.out.println("Headers: \n" + components.get("headers"));
        System.out.println("Body: " + components.get("body"));
        return components;
    }
}
