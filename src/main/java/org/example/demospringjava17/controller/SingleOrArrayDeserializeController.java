package org.example.demospringjava17.controller;

import lombok.extern.slf4j.Slf4j;
import org.example.demospringjava17.model.SingleOrArrayModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class SingleOrArrayDeserializeController {

    @PostMapping("/single")
    public ResponseEntity<String> single(@RequestBody SingleOrArrayModel req) {
        log.info("request: {}", req.getPortalJobId());
        return ResponseEntity.status(HttpStatus.OK).body("single ok");

    }

    @PostMapping("/array")
    public ResponseEntity<String> array(@RequestBody SingleOrArrayModel req) {
        log.info("request: {}", req.getPortalJobId());
        return ResponseEntity.status(HttpStatus.OK).body("array ok");

    }

}
