package org.example.demospringjava17.controller;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.example.demospringjava17.service.DownloadPdfService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.ByteArrayInputStream;

@RestController
@RequestMapping("/download")
@AllArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class DownloadPdfController {

    private DownloadPdfService downloadPdfService;

//    @GetMapping("/form")
//    public ResponseEntity<?> download(){
//        String result = downloadPdfService.downloadExistingPdfFile();
//        return ResponseEntity.ok().body(new DownloadPdfRequest(result));
//    }

    @RequestMapping(value = "/isr",
            method = RequestMethod.GET,
            produces = { "application/pdf"})
    public ResponseEntity<InputStreamResource> getAsInputStreamResourceEntity() throws Exception {

        HttpHeaders responseHeaders = new HttpHeaders();
//        if (accept.equals("text/csv")) {
        responseHeaders.set("Content-Disposition", "attachment; filename=resume-isr.pdf");
        byte[] result = downloadPdfService.downloadExistingPdfFileAsByte();
        return new ResponseEntity<>(new InputStreamResource(new ByteArrayInputStream(result)), responseHeaders, HttpStatus.OK);
//        } else {
//            responseHeaders.set("Content-Disposition", "attachment; filename=isr.pdf");
//            ByteArrayInputStream pdf = PdfSerializer.toPdf(people);
//            return new ResponseEntity<>(new InputStreamResource(pdf), responseHeaders, HttpStatus.OK);
//        }
    }

    @RequestMapping(value = "/bytes",
            method = RequestMethod.GET,
            produces = { "application/pdf"})
    public ResponseEntity<byte[]> getAsByteArrayEntity() throws Exception {
        byte[] result = downloadPdfService.downloadExistingPdfFileAsByte();
        HttpHeaders responseHeaders = new HttpHeaders();

//        responseHeaders.set("Content-Disposition", "attachment; filename=resume-bytes.pdf");
        return new ResponseEntity<>(result, responseHeaders, HttpStatus.OK);

    }

    @RequestMapping(value = "/async",
            method = RequestMethod.GET,
            produces = { "application/pdf"})
    public ResponseEntity<StreamingResponseBody> getAsStreamingResponseBody() throws Exception {
        byte[] result = downloadPdfService.downloadExistingPdfFileAsByte();
        HttpHeaders responseHeaders = new HttpHeaders();

        responseHeaders.set("Content-Disposition", "attachment; filename=resume-async.pdf");

        return new ResponseEntity<>(
                outputStream -> outputStream.write(result),
                responseHeaders,
                HttpStatus.OK);

    }

    @RequestMapping(value = "/servlet-response",
            method = RequestMethod.GET,
            produces = { "application/pdf"})
    public void writeToServletResponse(HttpServletResponse response) throws Exception {

        byte[] result = downloadPdfService.downloadExistingPdfFileAsByte();
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=resume-servlet-response.pdf");
        response.getOutputStream().write(result);


    }
}
