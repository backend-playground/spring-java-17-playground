package org.example.demospringjava17.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.demospringjava17.model.UploadFileRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@RestController
@Slf4j
@RequestMapping("/upload")
@AllArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class UploadPdfController {

    @PostMapping("/file")
    public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return ResponseEntity.badRequest().body("Please select a file to upload.");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body("File uploaded successfully.");

    }



    @PostMapping("/file-data")
    public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file,
                                                   @RequestParam("data") String requestBody) {
        if (file.isEmpty()) {
            return ResponseEntity.badRequest().body("Please select a file to upload.");
        }

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            UploadFileRequest requestBodyObj = objectMapper.readValue(requestBody, UploadFileRequest.class);

            int id = requestBodyObj.getId();
            String name = requestBodyObj.getName();
            log.info("requestBodyObj {}",requestBodyObj);
            log.info("ID: "+id);
            log.info("NAME: "+name);


            return ResponseEntity.status(HttpStatus.CREATED).body("File uploaded successfully. ID: " + id + ", Name: " + name);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to upload file: " + e.getMessage());
        }
    }
}
