package org.example.demospringjava17.controller;

import org.example.demospringjava17.model.CallApiRequest;
import org.example.demospringjava17.model.CallApiResponse;
import org.example.demospringjava17.service.CallApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class CallApiController {

    @Autowired
    private CallApiService callApiService;

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/call")
    @ResponseBody
    public CallApiResponse callApi(@RequestBody CallApiRequest callApiRequest){
        return callApiService.callApi(callApiRequest);
    }
}
