package org.example.demospringjava17;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringJava17Application {

    public static void main(String[] args) {
        SpringApplication.run(DemoSpringJava17Application.class, args);
    }

}
